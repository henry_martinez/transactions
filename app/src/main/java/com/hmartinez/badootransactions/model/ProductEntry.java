package com.hmartinez.badootransactions.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ProductEntry implements Parcelable {

    private String sku;

    private List<Transaction> transactions;

    public ProductEntry(String sku, List<Transaction> transactions) {

        this.sku = sku;
        this.transactions = transactions;
    }


    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sku);
        dest.writeTypedList(this.transactions);
    }

    protected ProductEntry(Parcel in) {
        this.sku = in.readString();
        this.transactions = in.createTypedArrayList(Transaction.CREATOR);
    }

    public static final Parcelable.Creator<ProductEntry> CREATOR = new Parcelable.Creator<ProductEntry>() {
        @Override
        public ProductEntry createFromParcel(Parcel source) {
            return new ProductEntry(source);
        }

        @Override
        public ProductEntry[] newArray(int size) {
            return new ProductEntry[size];
        }
    };
}
