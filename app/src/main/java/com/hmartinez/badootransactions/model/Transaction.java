package com.hmartinez.badootransactions.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction implements Parcelable {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("currency")
    @Expose
    private String currency;

    private String gbpEquivalent;


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGbpEquivalent() {
        return gbpEquivalent;
    }

    public void setGbpEquivalent(String gbpEquivalent) {
        this.gbpEquivalent = gbpEquivalent;
    }


    public Transaction() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.amount);
        dest.writeString(this.sku);
        dest.writeString(this.currency);
        dest.writeString(this.gbpEquivalent);
    }

    protected Transaction(Parcel in) {
        this.amount = in.readString();
        this.sku = in.readString();
        this.currency = in.readString();
        this.gbpEquivalent = in.readString();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel source) {
            return new Transaction(source);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };
}
