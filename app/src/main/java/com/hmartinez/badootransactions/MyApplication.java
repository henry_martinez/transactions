package com.hmartinez.badootransactions;

import android.app.Application;

import com.hmartinez.badootransactions.di.component.AppComponent;
import com.hmartinez.badootransactions.di.component.DaggerAppComponent;
import com.hmartinez.badootransactions.di.module.AppModule;


public class MyApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

       appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
