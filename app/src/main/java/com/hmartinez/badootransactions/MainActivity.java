package com.hmartinez.badootransactions;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hmartinez.badootransactions.databinding.ActivityMainBinding;
import com.hmartinez.badootransactions.managers.DataManager;
import com.hmartinez.badootransactions.model.ProductEntry;
import com.hmartinez.badootransactions.model.Transaction;
import com.hmartinez.badootransactions.ui.adapters.ProductsAdapter;
import com.hmartinez.badootransactions.ui.adapters.TransactionsAdapter;
import com.hmartinez.badootransactions.viewmodels.ProductViewModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.hmartinez.badootransactions.TransactionsActivity.TRANSACTIONS_ARG;

public class MainActivity extends AppCompatActivity implements ProductViewModel.ProductViewModelListener {

    private ActivityMainBinding viewDataBinding;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication) getApplication()).getAppComponent().inject(this);

        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initView();

        loadData();
    }

    private void initView() {
        viewDataBinding.recyclerViewProducts.setLayoutManager(new LinearLayoutManager(this));
    }

    private void loadData() {
        viewDataBinding.recyclerViewProducts.setAdapter(new ProductsAdapter(dataManager.loadTransation(), this));
    }


    @Override
    public void onProductSelected(ProductEntry entry) {
        Intent intent = new Intent(this, TransactionsActivity.class);
        intent.putExtra(TRANSACTIONS_ARG, entry);
        startActivity(intent);
    }
}
