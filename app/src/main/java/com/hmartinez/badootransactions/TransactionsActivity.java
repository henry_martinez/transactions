package com.hmartinez.badootransactions;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.hmartinez.badootransactions.databinding.ActivityTransactionsBinding;
import com.hmartinez.badootransactions.managers.ConversionRateManager;
import com.hmartinez.badootransactions.model.ProductEntry;
import com.hmartinez.badootransactions.model.Transaction;
import com.hmartinez.badootransactions.ui.adapters.TransactionsAdapter;
import com.hmartinez.badootransactions.viewmodels.TransactionsViewModel;

import java.util.List;

import javax.inject.Inject;

public class TransactionsActivity extends AppCompatActivity {

    public static final String TRANSACTIONS_ARG = "transactions";
    private ActivityTransactionsBinding viewDataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_transactions);

        viewDataBinding.recyclerViewTransactions.setLayoutManager(new LinearLayoutManager(this));

        final Bundle extras = getIntent().getExtras();
        if (extras != null)
       {
          ProductEntry entry =  extras.getParcelable(TRANSACTIONS_ARG);

           if (entry != null){
               final List<Transaction> transactions = entry.getTransactions();
               viewDataBinding.setViewModel(new TransactionsViewModel(transactions));
               viewDataBinding.recyclerViewTransactions.setAdapter(new TransactionsAdapter(transactions));
           }
       }
    }
}
