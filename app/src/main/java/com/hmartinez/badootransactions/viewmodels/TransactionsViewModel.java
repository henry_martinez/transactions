package com.hmartinez.badootransactions.viewmodels;


import android.databinding.ObservableField;

import com.hmartinez.badootransactions.model.Transaction;

import java.util.List;
import java.util.Locale;

import rx.Observable;

public class TransactionsViewModel {


    private List<Transaction> transactions;

    public ObservableField<String> transactionsTotal = new ObservableField<>();

    public TransactionsViewModel(List<Transaction> transactions) {

        this.transactions = transactions;

        loadTotal();
    }

    private void loadTotal() {
        double total = 0;
        for (int i = 0; i < transactions.size(); i++) {
            total += Double.valueOf(transactions.get(i).getGbpEquivalent());
        }

        transactionsTotal.set(String.format(Locale.getDefault(), "%.2f", total));
    }


}
