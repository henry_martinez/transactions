package com.hmartinez.badootransactions.viewmodels;

import android.databinding.ObservableField;

import com.hmartinez.badootransactions.model.Transaction;

import java.util.Locale;

public class TransactionViewModel {

    private final Transaction transaction;

    public ObservableField<String> gbpAmount = new ObservableField<>();

    public ObservableField<String> originalAmount = new ObservableField<>();


    public TransactionViewModel(Transaction transaction) {
        this.transaction = transaction;

        double amount = Double.parseDouble(transaction.getAmount());

        originalAmount.set(getFormat(amount, transaction.getCurrency()));

        double gbpAmountDouble = Double.parseDouble(transaction.getGbpEquivalent());
        this.gbpAmount.set(getFormat(gbpAmountDouble, "GBP"));
    }

    private String getFormat(double amount, String currency) {
        return String.format(Locale.getDefault(), "%.2f " + currency, amount);
    }


}
