package com.hmartinez.badootransactions.viewmodels;

import android.databinding.ObservableField;

import com.hmartinez.badootransactions.model.ProductEntry;

public class ProductViewModel {

    private final ProductEntry productEntry;

    public ObservableField<String> productSKU = new ObservableField<>();

    public ObservableField<String> productQuantity = new ObservableField<>();

    private ProductViewModelListener listerner;



    public ProductViewModel(ProductEntry product) {
        this.productEntry = product;

        productSKU.set(product.getSku());
        productQuantity.set(String.valueOf(product.getTransactions().size()));
    }

    public void onProductSelected(){
        listerner.onProductSelected(productEntry);
    }

    public void setListener(ProductViewModelListener listerner) {
        this.listerner = listerner;
    }

    public interface ProductViewModelListener
    {
        public void onProductSelected(ProductEntry entry);
    }


}
