package com.hmartinez.badootransactions.managers;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hmartinez.badootransactions.Utils;
import com.hmartinez.badootransactions.model.ProductEntry;
import com.hmartinez.badootransactions.model.Rate;
import com.hmartinez.badootransactions.model.Transaction;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataManager {

    private Context context;
    private final ConversionRateManager conversionRateManager;

    public DataManager(Context context) {
        this.context = context;

        conversionRateManager = new ConversionRateManager(loadRates());
    }

    public List<ProductEntry> loadTransation(){

        List<ProductEntry> productEntries = new ArrayList<>();

        final String jsonFromAsset = Utils.loadJSONFromAsset(context, "transactions.json");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Transaction>>(){}.getType();
        List<Transaction> transactions = gson.fromJson(jsonFromAsset, listType);


        if (transactions != null && !transactions.isEmpty()){
            Map<String, List<Transaction>> mapTransactions = getTransactionMap(transactions);

            for (Map.Entry<String, List<Transaction>> entry : mapTransactions.entrySet())
            {
                productEntries.add(new ProductEntry( entry.getKey(), entry.getValue()));
            }
        }

        return productEntries;
    }

    public List<Rate> loadRates(){

        List<Rate> rateList;

        final String jsonFromAsset = Utils.loadJSONFromAsset(context, "rates.json");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Rate>>(){}.getType();
        rateList = gson.fromJson(jsonFromAsset, listType);

        return rateList;
    }


    @NonNull
    private Map<String, List<Transaction>> getTransactionMap(List<Transaction> transactionRaw) {
        Map<String, List<Transaction>> mapTransactions = new HashMap<>();
        final int size = transactionRaw.size();
        for (int i = 0; i < size; i++) {
            final Transaction transaction = transactionRaw.get(i);
            final String sku = transaction.getSku();
            List<Transaction> transactions = mapTransactions.get(sku);
            if (transactions == null) {
                transactions = new ArrayList<>();
            }

            transaction.setGbpEquivalent(String.valueOf(conversionRateManager.convertToGBP(transaction.getAmount(), transaction.getCurrency())));
            transactions.add(transaction);
            mapTransactions.put(sku, transactions);
        }
        return mapTransactions;
    }
}
