package com.hmartinez.badootransactions.managers;

import com.hmartinez.badootransactions.model.Rate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConversionRateManager {

   private List<Rate> rates;
    private Map<String, Rate> directRates;

    private Map<String, List<Rate>> indirectRates;


    public ConversionRateManager( List<Rate> rates) {
        this.rates = rates;
        loadDirectRates();
    }


    private void loadDirectRates(){
        directRates = new HashMap<>();
        indirectRates = new HashMap<>();
        if (rates != null ) {
            for (int i = 0; i < rates.size() ; i++) {
                final Rate rate = rates.get(i);
                if ( rate.getTo().equals("GBP")) {
                   directRates.put(rates.get(i).getFrom(),rates.get(i));
                } else {

                    List<Rate> tempRates = indirectRates.get(rates.get(i).getFrom());
                    if (tempRates == null) {
                        tempRates = new ArrayList<>();
                    }
                    tempRates.add(rates.get(i));

                    indirectRates.put(rates.get(i).getFrom(), tempRates);
                }
            }
        }
    }

    public double convertToGBP(String amount, String currency){
        return convertToGBP(Double.valueOf(amount), currency);
    }


    public double convertToGBP(double amount, String currency){
        double conversion = -1;

        final double rate = getRate(currency);
        if (rate != -1){
            conversion = amount * rate;
        }

        return conversion;
    }


    public double getRate(String currency){


        double rateValue = -1;

        if (currency.equals("GBP")){
            return 1;
        } else if ( directRates.containsKey(currency)){
            return Double.parseDouble(directRates.get(currency).getRate());
        } else if (indirectRates.containsKey(currency)){
            final List<Rate> rates = indirectRates.get(currency);
             Rate middleRate= null;
            for (int i = 0; i < rates.size(); i++) {
                if (directRates.containsKey(rates.get(i).getTo())){
                    middleRate = rates.get(i);
                    break;
                }
            }
            if (middleRate != null){
                return Double.parseDouble(middleRate.getRate()) * Double.parseDouble(directRates.get(middleRate.getTo()).getRate());

            }
        }

        return rateValue;
    }



}
