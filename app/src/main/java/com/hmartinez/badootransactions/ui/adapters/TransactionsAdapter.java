package com.hmartinez.badootransactions.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hmartinez.badootransactions.R;
import com.hmartinez.badootransactions.databinding.ItemProductBinding;
import com.hmartinez.badootransactions.databinding.ItemTransactionBinding;
import com.hmartinez.badootransactions.managers.ConversionRateManager;
import com.hmartinez.badootransactions.model.ProductEntry;
import com.hmartinez.badootransactions.model.Transaction;
import com.hmartinez.badootransactions.viewmodels.ProductViewModel;
import com.hmartinez.badootransactions.viewmodels.TransactionViewModel;

import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    List<Transaction> transactions;
    private ProductViewModel.ProductViewModelListener listener;
    private ItemTransactionBinding binding;


    public TransactionsAdapter(List<Transaction> transactionList) {
        this.transactions = transactionList;
    }

    @Override
    public TransactionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.item_transaction, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(TransactionsAdapter.ViewHolder viewHolder, int position) {
        Transaction productEntry = transactions.get(position);
        binding.setViewModel(new TransactionViewModel(productEntry));
        binding.executePendingBindings();
    }
    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {

            super(itemView);
        }
    }
}