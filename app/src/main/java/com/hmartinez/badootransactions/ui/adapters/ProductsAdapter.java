package com.hmartinez.badootransactions.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hmartinez.badootransactions.R;
import com.hmartinez.badootransactions.databinding.ItemProductBinding;
import com.hmartinez.badootransactions.model.ProductEntry;
import com.hmartinez.badootransactions.viewmodels.ProductViewModel;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    List<ProductEntry> productEntries;
    private ProductViewModel.ProductViewModelListener listener;
    private ItemProductBinding binding;


    public ProductsAdapter(List<ProductEntry> transactionMap, ProductViewModel.ProductViewModelListener listerner) {
        this.productEntries = transactionMap;
        this.listener = listerner;
    }

    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.item_product, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ViewHolder viewHolder, int position) {
        ProductEntry productEntry = productEntries.get(position);
        final ProductViewModel viewModel = new ProductViewModel(productEntry);
        viewModel.setListener(listener);
        binding.setViewModel(viewModel);
        binding.executePendingBindings();
    }
    @Override
    public int getItemCount() {
        return productEntries.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {

            super(itemView);
        }
    }
}