package com.hmartinez.badootransactions.di.module;

import android.app.Application;
import android.content.Context;

import com.hmartinez.badootransactions.managers.ConversionRateManager;
import com.hmartinez.badootransactions.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public Context appContext() {
        return mApplication;
    }


    @Provides
    @Singleton
    DataManager dataManager(Context context){
        return new DataManager(context);
    }


}