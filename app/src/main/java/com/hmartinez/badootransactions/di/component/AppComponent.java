package com.hmartinez.badootransactions.di.component;

import com.hmartinez.badootransactions.MainActivity;
import com.hmartinez.badootransactions.di.module.AppModule;
import com.hmartinez.badootransactions.managers.DataManager;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {

    DataManager dataManager();

    void inject(MainActivity mainActivity);
}